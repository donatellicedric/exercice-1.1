package fr.cnam.foad.nfa035.fileutils.simpleaccess;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Base64;

public class ImageSerializerBase64Impl implements ImageSerializer {
	
	
/**
 * serialise une image
 * @param image
 * @throws IOException
 * @return une liste de lettre qui représente l'image encodedImage
 */
	@Override
	public String serialize(File image) throws IOException {
		byte[] fichier = Files.readAllBytes(image.toPath());
		String encodedImage = Base64.getEncoder().encodeToString(fichier);
		return encodedImage;
	}
	/**
	 * @param image
	 * deserialise une image
	 * @return decodedImage en byte
	 */
	@Override
	public byte[] deserialize(String image) {
		byte[] decodedImage = Base64.getDecoder().decode(image);
		return decodedImage;
	}
	

}

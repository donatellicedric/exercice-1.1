package fr.cnam.foad.nfa035.fileutils.simpleaccess;

import java.io.File;
import java.io.IOException;

public interface ImageSerializer {
	
/**
 * seriale une image
 * @param image
 * @return
 * @throws IOException 
 */
	String serialize(File image) throws IOException;
/**
 * deserialise une image
 * @param encodedImage
 * @return
 */
	byte[] deserialize(String encodedImage);

}
